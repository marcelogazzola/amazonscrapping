from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


def searchKeyword(keyword):
    fieldKeyword = browser.find_element_by_name('field-keywords')
    fieldKeyword.send_keys(keyword)
    fieldKeyword.send_keys(Keys.ENTER)


def searchTitle(title):
    fieldKeyword = browser.find_element_by_name('field-title')
    fieldKeyword.send_keys(title)
    fieldKeyword.send_keys(Keys.ENTER)

    prevPage = browser.current_url
    url = prevPage
    getBooks()
    
    while url != '':
        browser.get(prevPage)

        nextPg = browser.find_elements_by_class_name('a-text-center')
        ul = nextPg[0].find_element_by_tag_name('ul')
        li = ul.find_elements_by_tag_name('li')

        url = ''
        for i in li:
            if i.get_attribute('class') == 'a-last':
                nx = i.find_element_by_tag_name('a')
                url = nx.get_attribute('href')
                browser.get(url)

        if url != '':
            prevPage = browser.current_url
            getBooks()


def getBooks():
    books = set()
    divs = browser.find_elements_by_tag_name('div')
    for div in divs:
        try:
            if div.get_attribute('data-component-type') == 's-search-result': 
                link = div.find_element_by_tag_name('a')
                books.add(link.get_attribute('href'))
        except:
            continue

    for book in books:
        try:
            browser.get(book)
            detail = browser.find_element_by_id('detail_bullets_id')
            ul = detail.find_element_by_tag_name('ul')
            li = ul.find_elements_by_tag_name('li')
            for item in li:
                print(item.text)
        except:
            continue



chrome_options = Options()
chrome_options.add_argument("--disable-notifications")
chrome_options.add_argument("--disable-popup-blocking")
chrome_options.add_argument("start-minimized")

browser = webdriver.Chrome(chrome_options=chrome_options)

browser.get('https://www.amazon.co.uk/Book-Search-Books/b?ie=UTF8&node=125552011')

searchTitle('Cooking')