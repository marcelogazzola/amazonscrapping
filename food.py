import time
import re
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


class Product:
    def __init__(self, asin, url, name, companyName, lowestPrice, flavorName, sizeUnitCount, ingredients, safetyWarning, category, upc, modelNumber, rating,
                ratings, mainPicUrl, nutritionPicUrl, available, availablePrime, availableAmazonFresh, availableAmazonPantry, availableAmazonWholeFoodsMarket, availableSubscribeSave):

        self.asin = asin
        self.url = url
        self.name = name
        self.companyName = companyName
        self.lowestPrice = lowestPrice
        self.flavorName = flavorName
        self.sizeUnitCount = sizeUnitCount
        self.ingredients = ingredients
        self.safetyWarning = safetyWarning
        self.category = category
        self.upc = upc
        self.modelNumber = modelNumber
        self.rating = rating
        self.ratings = ratings
        self.mainPicUrl = mainPicUrl
        self.nutritionPicUrl = nutritionPicUrl
        self.available = available
        self.availablePrime = availablePrime
        self.availableSubscribeSave = availableSubscribeSave
        self.availableAmazonPantry = availableAmazonPantry
        self.availableAmazonFresh = availableAmazonFresh
        self.availableAmazonWholeFoodsMarket = availableAmazonWholeFoodsMarket
    
    def to_csv(self):
        attrs = self.__dict__.items()
        produto = ''
        for attr, value in attrs:
            print(attr, value)
            produto = produto + ',' + value

        f = open('products.csv', 'a+')
        f.write(produto)


def groceries():
    chrome_options = Options()
    chrome_options.add_argument("--disable-notifications")
    chrome_options.add_argument("--disable-popup-blocking")
    chrome_options.add_argument("start-minimized")
    # chrome_options.add_argument('headless')
    browser = webdriver.Chrome(options=chrome_options)

    browser.get('https://www.amazon.com/gp/browse.html?node=16310101')

    browser.implicitly_wait(5)

    # List of subcategories
    categories = browser.find_element_by_xpath('//*[@id="leftNav"]/ul[1]/ul/div')
    categories = categories.find_elements_by_tag_name('li')
    categoriesUrl = []
    for cat in categories:
        categoriesUrl.append(cat.find_element_by_tag_name('a').get_attribute('href'))
    categoriesUrl.pop(9)
    
    # Navigate through each sub category
    for category in categoriesUrl:
        browser.get(category)
        time.sleep(3)
        getProductInfo(browser)


def getProductInfo(browser):
    # Create list of products urls
    products = browser.find_element_by_xpath('//*[@id="mainResults"]/ul')
    products = products.find_elements_by_tag_name('li')
    productsUrl = []
    for product in products:
        try:
            url = product.find_element_by_tag_name('a').get_attribute('href')
            if 'dp/' in url: productsUrl.append(url)
        except:
            continue

    for productUrl in productsUrl:
        browser.get(productUrl)

        url = browser.current_url
        name = browser.find_element_by_id('productTitle').text
        company = browser.find_element_by_id('bylineInfo').text

        # ASIN / UPC / Reviews
        details = browser.find_element_by_id('detail-bullets')
        items = details.find_elements_by_tag_name('b')
        for item in items:
            if item.text == 'ASIN:': asin = item.find_element_by_xpath('..').text
            if item.text == 'UPC:': upc = item.find_element_by_xpath('..').text
            if item.text == 'Customer Reviews:': reviews = item.find_element_by_xpath('..').text
            if item.text == 'Item model number:': itemNumber = item.find_element_by_xpath('..').text
        

        # Size
        try:
            size = browser.find_element_by_id('variation_size_name').text
        except:
            size = ''

        # Rating
        try:
            rating = browser.find_element_by_id('acrPopover').text
        except:
            rating = ''

        # Category
        try:
            category = browser.find_element_by_id('wayfinding-breadcrumbs_feature_div').text.replace('\n','')
        except:
            category = ''
        
        # Flavor
        try:
            flavor = browser.find_element_by_id('variation_flavor_name').text
        except:
            flavor = ''

        # Availability
        try:
            availability = browser.find_element_by_id('availability').text
        except:
            availability = ''

        try: 
            availableSubscribeSave = browser.find_element_by_id('snsBuyBox')
            availableSubscribeSave = 'Yes'
        except: 
            availableSubscribeSave = ''
        try: 
            availableAmazonPantry = browser.find_element_by_id('merchant-info').text 
        except: 
            availableAmazonPantry = ''
        try: 
            availableAmazonFresh = 'availableAmazonFresh' 
        except: 
            availableAmazonFresh = ''
        availableAmazonWholeFoodsMarket = ''

        # category = list inside id = wayfinding-breadcrumbs_feature_div
        name = browser.find_element_by_id('productTitle').text

        # Get offers to get the lowest price
        code = re.findall('dp/(.*?)/ref', url)
        offersUrl = 'https://www.amazon.com/gp/offer-listing/{}/ref=dp_olp_new?ie=UTF8&condition=new'.format(code[0])
        browser.get(offersUrl)

        # Lowest Price and Available Prime
        price = browser.find_element_by_xpath('//*[@id="olpOfferList"]/div/div/div[2]/div[1]/span[1]').text
        availablePrime = browser.find_element_by_class_name('supersaver')
        availablePrime = availablePrime.find_elements_by_tag_name('i')
        if len(availablePrime) > 0: availablePrime = 'Yes'

        product = Product(
            asin=asin,
            url=url,
            name=name,
            companyName=company,
            lowestPrice=price,
            flavorName=flavor,
            sizeUnitCount=size,
            ingredients='',
            safetyWarning='',
            category=category,
            upc=upc,
            modelNumber=itemNumber,
            rating = 'rating',
            ratings = reviews,
            mainPicUrl = 'mainPicUrl',
            nutritionPicUrl = 'nutritionPicUrl',
            available = availability,
            availablePrime = availablePrime,
            availableSubscribeSave = availableSubscribeSave,
            availableAmazonPantry = 'availableAmazonPantry',
            availableAmazonFresh = 'availableAmazonFresh',
            availableAmazonWholeFoodsMarket = 'availableAmazonWholeFoodsMarket'

        )

        product.to_csv()

        print('a')

if __name__ == "__main__":
    groceries()